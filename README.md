# patches

Android studio patches for system projects.

Inside Android Studio: Git -> Patch -> Apply patch

Apply the patches in order. Make sure to not commit these patch induced changes and only commit your own changes.
